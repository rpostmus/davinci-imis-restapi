﻿using DavinciRestAPI.Filters;
using DavinciRestAPI.Helpers;
using DavinciRestAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;

namespace DavinciRestAPI.Controllers
{
    [RequireHttps]
    public class DavinciApiController : ApiController
    {
        /// <summary>
        /// Get Token
        /// /api/davinciapi?Username=Plaap&password=test
        /// </summary>
        /// <param name="username">Username for API request</param>
        /// <param name="password">Password for API request</param>
        /// <returns>if valid returns the authentication token. If not valid returns exception</returns>
        [AllowAnonymous]
        [HttpGet]
        ///api/davinciapi
        public string Get(string username, string password)
        {
            if (CheckUser(username, password))
            {
                return JwtManager.GenerateToken(username);
            }

            throw new HttpResponseException(HttpStatusCode.Unauthorized); ;
        }

        private bool CheckUser(string username, string password)
        {
            //We use iMIS OAUTH2 but do not use the token. We use our own token.
            return IMISConnectionHelper.AuthenticateWithIMIS(username,password);
        }
    }
}
