﻿using DavinciRestAPI.Filters;
using DavinciRestAPI.Helpers;
using DavinciRestAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DavinciRestAPI.Controllers
{
    /// <summary>
    /// This controller handles all the 'sectioned' requests to the API, as prefixed by 'v2'
    /// </summary>
    public class GeneralController : ApiController
    {
        [HttpGet]
        [JwtAuthentication]
        public HttpResponseMessage Get(int plaap = 0)
        {
            //Check endpoint
            var endpoint = EndpointHelper.GetValidEndPointController(ActionContext.Request);

            if (endpoint == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            //Hier database logica
            //var response = "{\"ActieCode\": 15338,\"Name\": \"Test campaign\",\"Motivation\": \"Omschrijving van de actie\",\"TypeActie\": \"Creatief\",\"TargetAmount\": 1000.00,\"CurrentAmount\": 986.45,\"DonationCount\": 45,\"Status\": \"2\",\"IsTeam\": false,\"IsVisible\": true,\"CreatedDate\": \"2017-05-29T09:00:53\",\"ActieStartDate\": \"2017-06-12T09:00:53\"}";
            var result = DavinciDBHelper.ExecuteProcedure(Request, endpoint);

            //return Request.CreateResponse(HttpStatusCode.OK, tset);
            return result;
        }

        // GET: api/DavinciApi/5
        [HttpGet]
        [JwtAuthentication]
        public HttpResponseMessage Get(string id)
        {
            var endpoint = EndpointHelper.GetValidEndPointController(ActionContext.Request);

            if (endpoint == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            //parameters checken
            endpoint.Parameters.Where(x => x.ParameterType == ParameterType.PrimaryKey).Select(y => y.Value = id).ToList();
            //if (endpoint.Parameters.Any(x => x.ParameterType == ParemeterType.PrimaryKey))
            //    endpoint.Parameters.Where(x =>)

            //Database stuff::
            var result = DavinciDBHelper.ExecuteProcedure(Request, endpoint);


            return result;
        }

        // POST: api/DavinciApi
        [HttpPost]
        [JwtAuthentication]
        public HttpResponseMessage Post([FromBody]JToken dto)
        {
            var endpoint = EndpointHelper.GetValidEndPointController(ActionContext.Request);

            if (endpoint == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            endpoint.Body = JsonConvert.SerializeObject(dto);

            //Database stuff::
            return DavinciDBHelper.ExecuteProcedure(Request, endpoint);
        }


        // PUT: api/DavinciApi/5
        [HttpPut]
        [ActionName("put")]
        [JwtAuthentication]
        public HttpResponseMessage Put([FromUri] string id, [FromBody]JToken dto)
        {
            var endpoint = EndpointHelper.GetValidEndPointController(ActionContext.Request);

            if (endpoint == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            endpoint.Body = JsonConvert.SerializeObject(dto);

            //Database stuff::
            return DavinciDBHelper.ExecuteProcedure(Request, endpoint, id);
        }

        // DELETE: api/DavinciApi/5
        [HttpDelete]
        [ActionName("delete")]
        [JwtAuthentication]
        public HttpResponseMessage Delete(string id)
        {
            var endpoint = EndpointHelper.GetValidEndPointController(ActionContext.Request);

            if (endpoint == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            //Database stuff::
            return DavinciDBHelper.ExecuteProcedure(Request, endpoint, id);
        }

    }
}