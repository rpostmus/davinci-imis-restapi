﻿namespace DavinciRestAPI.Models
{
    public enum ParameterType
    {
        PrimaryKey,
        ForeignKey,
        None
    }
}