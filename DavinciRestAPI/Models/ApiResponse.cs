﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DavinciRestAPI.Models
{
    public class ApiResponse
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public string Response { get; set; }
    }
}