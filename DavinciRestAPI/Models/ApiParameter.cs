﻿using DavinciRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DavinciRestAPI.Models
{
    public class ApiParameter
    {
        public ParameterType ParameterType { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}