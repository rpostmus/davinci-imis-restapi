﻿using DavinciRestAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DavinciRestAPI.Models
{
    public class ApiToStoredProcedure
    {
        public string Endpoint { get; set; }

        public string Method { get; set; }

        public string Procedure { get; set; }

        public List<ApiParameter> Parameters { get; set; }

        public string Body { get; set; }

        public APIType apiType { get; set; }
    }
}