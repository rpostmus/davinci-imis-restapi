﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DavinciRestAPI.Helpers
{
    public enum APIType
    {
        Actieplatform = 1,
        DonatieForm = 2,
        ChangePersonalInformationForm = 3,
        Eventplatform = 4,
        Questionnaire = 5
    }

    public static class ApiNameHelper
    {
        public const string Actieplatform = "actieplatform";
        public const string DonatieForm = "donatieformulier";
        public const string ChangePersonalInformationForm = "informatieformulier";
        public const string Eventplatform = "evenementplatform";
        public const string Questionnaire = "questionnaire";

        public static string EndpointForAPIType(APIType type)
        {
            switch (type)
            {
                case APIType.Actieplatform:
                    return ApiNameHelper.Actieplatform;
                case APIType.ChangePersonalInformationForm:
                    return ApiNameHelper.ChangePersonalInformationForm;
                case APIType.DonatieForm:
                    return ApiNameHelper.DonatieForm;
                case APIType.Eventplatform:
                    return ApiNameHelper.Eventplatform;
                case APIType.Questionnaire:
                    return ApiNameHelper.Questionnaire;
                default:
                    throw new KeyNotFoundException($"No endpoint defined for API Type {type.ToString()}");
            }
        }

        public static APIType APITypeForEndpoint(string endpoint)
        {
            switch (endpoint)
            {
                case ApiNameHelper.Actieplatform:
                    return APIType.Actieplatform;
                case ApiNameHelper.ChangePersonalInformationForm:
                    return APIType.ChangePersonalInformationForm;
                case ApiNameHelper.DonatieForm:
                    return APIType.DonatieForm;
                case ApiNameHelper.Eventplatform:
                    return APIType.Eventplatform;
                case ApiNameHelper.Questionnaire:
                    return APIType.Questionnaire;
                default:
                    throw new KeyNotFoundException($"Unknown endpoint '{endpoint}'");
            }
        }
    }
}