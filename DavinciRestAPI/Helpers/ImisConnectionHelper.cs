﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace DavinciRestAPI.Helpers
{
    public static class IMISConnectionHelper
    {
        //Use iMIS oauth method.
        public static bool AuthenticateWithIMIS(string username, string password)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

            var client = new HttpClient();
            // Format headers
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            // POST token request with credentials
            var asiScheduler = ConfigurationManager.AppSettings["schedulerUrl"];

            var response = client.PostAsync(asiScheduler + "Token",
                new FormUrlEncodedContent(
                new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", username),
                    new KeyValuePair<string, string>("password", password),

                })).Result;
            // Deserialize JSON response to token class
            var token = response.Content.ReadAsAsync<Token>();
            if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(token.Result.AccessToken))
            {
                return true;
            }
            return false;
        }

        private class Token
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }
        }
    }
}