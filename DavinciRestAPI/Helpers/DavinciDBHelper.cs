﻿using DavinciRestAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Web;

namespace DavinciRestAPI.Helpers
{
    public class DavinciDBHelper
    {
        public static HttpResponseMessage ExecuteProcedure(HttpRequestMessage request, ApiToStoredProcedure apiToStoredProcedure)
        {
            return ExecuteProcedure(request, apiToStoredProcedure, string.Empty);
        }
        public static HttpResponseMessage ExecuteProcedure(HttpRequestMessage request, ApiToStoredProcedure apiToStoredProcedure, string id)
        {

            var connString = ConfigurationManager.ConnectionStrings["imisconnectionstring"].ConnectionString;
            if(apiToStoredProcedure.apiType == APIType.DonatieForm || apiToStoredProcedure.apiType == APIType.ChangePersonalInformationForm || apiToStoredProcedure.apiType == APIType.Questionnaire)
                connString = ConfigurationManager.ConnectionStrings["imisdonatieconnectionstring"].ConnectionString;
            if (apiToStoredProcedure.apiType == APIType.Eventplatform)
                connString = ConfigurationManager.ConnectionStrings["imiseventsconnectionstring"].ConnectionString;


            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(apiToStoredProcedure.Procedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    //Queryparameters meesturen
                    //We pakken de query parameters die in de request zitten
                    var qParams = HttpUtility.ParseQueryString(request.RequestUri.Query);
                    // als er parameters in de request horen voegen we deze toe
                    if (apiToStoredProcedure.Parameters != null)
                    {
                        foreach (var item in apiToStoredProcedure.Parameters)
                        {
                            if (item.ParameterType == ParameterType.PrimaryKey && !string.IsNullOrEmpty(id))
                            {
                                if (apiToStoredProcedure.Body == null)
                                {
                                    qParams.Add(item.Name, id);
                                    continue;
                                }
                                    
                                JObject jo = JObject.FromObject(JsonConvert.DeserializeObject(apiToStoredProcedure.Body));

                                if(!jo.ContainsKey(item.Name))
                                    jo.Add(item.Name, id);

                                apiToStoredProcedure.Body = JsonConvert.SerializeObject(jo);
                                //add to query string
                                qParams.Add(item.Name, id);
                            }                                
                            else
                                qParams.Add(item.Name, item.Value);
                        }
                    }

                    //We always use json,httpstatuscode and returnvalue parameter for response
                    //json = the object we return, can be null
                    //httpstatuscode = is the status code we return
                    //returnvalue = if we get sql error it will be in this parameter.
                    cmd.Parameters.AddWithValue("@p_method", request.Method.Method.ToString());
                    cmd.Parameters.AddWithValue("@p_QueryParams", qParams.ToString());
                    cmd.Parameters.AddWithValue("@p_requestJSON", string.IsNullOrEmpty(apiToStoredProcedure.Body) ? "" : apiToStoredProcedure.Body);
                    cmd.Parameters.Add(new SqlParameter("@p_replyJSON", SqlDbType.NVarChar, 2147483641) { Direction = ParameterDirection.Output });
                    //cmd.Parameters.Add(new SqlParameter("@p_httpstatuscode", SqlDbType.NVarChar, 3) { Direction = ParameterDirection.Output });
                    var returnValue = new SqlParameter("@p_ReturnValue", DbType.Int32) { Direction = ParameterDirection.ReturnValue };
                    cmd.Parameters.Add(returnValue);

                    // open connection and execute stored procedure
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        //create ApiResponse object
                        var apiResponse = new ApiResponse();

                        apiResponse.ErrorMessage = e.Message;
                        apiResponse.IsSuccess = false;
                        apiResponse.Response = string.Empty;

                        //direclty return response to prevent further execution of this method
                        return request.CreateResponse(HttpStatusCode.InternalServerError, apiResponse);
                    }


                    // read output value from @NewId cmd.Parameters["@p_Json"].Value
                    var jsonObject = cmd.Parameters["@p_replyJSON"].Value.ToString();
                    //jsonObject = jsonObject.Replace("\"", "'");

                    var response = request.CreateResponse(returnValue.Value.ToString() != "0" ? HttpStatusCode.BadRequest : HttpStatusCode.OK);

                    //return request.CreateResponse(HttpStatusCode.BadRequest, jsonObject, JsonMediaTypeFormatter.DefaultMediaType);
                                        
                    response.Content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
                    return response;
                    //return request.CreateResponse(HttpStatusCode.OK, jsonObject, JsonMediaTypeFormatter.DefaultMediaType);
                }

            }
        }
    }
}
