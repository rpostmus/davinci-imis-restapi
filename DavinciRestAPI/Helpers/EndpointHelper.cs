﻿using DavinciRestAPI.Controllers;
using DavinciRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace DavinciRestAPI.Helpers
{
    public static class EndpointHelper
    {
        public static ApiToStoredProcedure GetValidEndPointController(HttpRequestMessage request)
        {
            //var principal = ActionContext.RequestContext.Principal.Identity;
            var token = request.Headers.Authorization.Parameter;
            var principal = JwtManager.GetPrincipal(token);
            var endpoint = request.GetRouteData().Values;
            var controller = endpoint["section"].ToString().ToLower();
            var action = endpoint["resource"];
            var method = request.Method;

            APIType apiType;
            switch (controller)
            {
                case ApiNameHelper.Eventplatform:
                    if (principal.IsInRole(UserRoles.EventAdministrator))
                    {
                        apiType = APIType.Eventplatform;
                        break;
                    }
                    else
                    {
                        return null;
                    }
                default:
                    apiType = ApiNameHelper.APITypeForEndpoint(controller);
                    break;
            }

            return GetEndpointSet(apiType, action.ToString(), method.ToString());
        }

        public static ApiToStoredProcedure GetEndpointSet(APIType apiType, string endpoint, string method)
        {
            return EndpointSet(apiType).Where(e => e.Endpoint.ToLower() == endpoint.ToLower() && e.Method.ToLower() == method.ToLower()).SingleOrDefault();
        }

        /// <summary>
        /// Predefined list of endpoints that we allow.
        /// </summary>
        /// <returns>List with endpoints</returns>
        public static IEnumerable<ApiToStoredProcedure> EndpointSet(APIType apiType = APIType.Actieplatform)
        {
            var dataset = new List<ApiToStoredProcedure>();

            if (apiType == APIType.ChangePersonalInformationForm)
            {
                dataset = new List<ApiToStoredProcedure>()
                {
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "EditDonation",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessEditDonationRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "EditBankAccount",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessEditBankAccountRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "EditAddress",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessEditAddressRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "EditEmail",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessEditEmailRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "CancelDonation",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessCancelDonationRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "CancelEmail",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessCancelEmailRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "CancelMail",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessCancelMailRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "CancelPhone",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessCancelPhoneRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Deceased",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessDeceasedRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Questionnaire",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessQuestionnaire",
                        apiType = apiType
                    }
                };
            }

            if (apiType == APIType.DonatieForm)
            {
                dataset = new List<ApiToStoredProcedure>()
                {
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Donatie",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessDonatieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "DonatieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Donatie",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessDonatieRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Donatie",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessDonatieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "DonatieId", Value = "" } },
                        apiType = apiType
                    }
                };
            }

            if (apiType == APIType.Actieplatform)
            {
                dataset = new List<ApiToStoredProcedure>()
                {
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Overzicht",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessOverzichtRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Acties",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessActiesRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Actie",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessActieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ActieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Actie",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessActieRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Actie",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessActieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "ActieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betalingen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessBetalingenRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betaling",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessBetalingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "BetalingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betaling",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessBetalingRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betaling",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessBetalingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "BetalingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ActieCampagne",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessActieCampagneRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "ActieCampagneId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ActieCampagnes",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessActieCampagnesRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ActieCampagne",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessActieCampagneRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ActieCampagne",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessActieCampagneRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "ActieCampagneId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ActieBezoek",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessActieBezoekRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ActieBezoekId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ActieBezoek",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessActieBezoekRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ActieBezoek",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessActieBezoekRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ActieBezoekId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Teams",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessTeamsRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Team",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessTeamRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "TeamId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Team",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessTeamRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Team",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessTeamRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "TeamId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Members",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessRelatiesRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Member",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessRelatieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "RelatieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Member",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessRelatieRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Member",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessRelatieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "RelatieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "AddMemberToActie",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessRelatieToActieRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Versterker",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessVersterkerRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Versterkers",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessVersterkersRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "VersterkingsVraagId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "VersterkingsVraag",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessVersterkingsVraagRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "VersterkingsVraag",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessVersterkingsVraagRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "VersterkingsVraagId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "VersterkingsVraag",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessVersterkingsVraagRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "VersterkingsVraagId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "VersterkingsVragen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessVersterkingsVragenRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVragen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessExtraVragenRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ExtraVragenId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVragenAntwoord",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessExtraVragenAntwoordRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ExtraVragenAntwoordId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVragenAntwoord",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessExtraVragenAntwoordRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ExtraVragenAntwoordId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVragenAntwoord",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessExtraVragenAntwoordRequest",
                        apiType = apiType
                    }
                };
            }

            if (apiType == APIType.Eventplatform)
            {
                dataset = new List<ApiToStoredProcedure>()
                {
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Deelnames",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessDeelnamesRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Deelname",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessDeelnameRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "DeelnameId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Deelname",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessDeelnameRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Deelname",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessDeelnameRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "DeelnameId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betalingen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessBetalingenRequest",
                        Parameters = new List<ApiParameter>()
                        {
                            new ApiParameter() { ParameterType = ParameterType.ForeignKey, Name = "DeelnameId", Value = ""},
                            new ApiParameter() { ParameterType = ParameterType.ForeignKey, Name = "TeamId", Value = "" }
                        },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betaling",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessBetalingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "BetalingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betaling",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessBetalingRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Betaling",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessBetalingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "BetalingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Editie",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessEditieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "EditieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Edities",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessEditiesRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Editie",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessEditieRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Editie",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessEditieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "EditieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "DeelnameBezoek",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessDeelnameBezoekRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "DeelnameBezoekId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "DeelnameBezoek",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessDeelnameBezoekRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "DeelnameBezoek",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessDeelnameBezoekRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "DeelnameBezoekId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Teams",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessTeamsRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Team",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessTeamRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "TeamId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Team",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessTeamRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Team",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessTeamRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "TeamId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Relaties",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessRelatiesRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Relatie",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessRelatieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "RelatieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Relatie",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessRelatieRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Relatie",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessRelatieRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "RelatieId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "AddMemberToActie",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessRelatieToActieRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVragen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessExtraVragenRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVraag",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessExtraVraagRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ExtraVraagId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVraag",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessExtraVraagRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVraag",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessExtraVraagRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ExtraVraagId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVraagAntwoorden",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessExtraVraagAntwoordenRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVraagAntwoord",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessExtraVraagAntwoordRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ExtraVraagAntwoordId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVraagAntwoord",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessExtraVraagAntwoordRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "ExtraVraagAntwoordId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "ExtraVraagAntwoord",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessExtraVraagAntwoordRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Uitdagingen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessUitdagingenRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Uitdaging",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessUitdagingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "UitdagingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Uitdaging",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessUitdagingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "UitdagingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Uitdaging",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessUitdagingRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "TeamUitnodigingen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessTeamUitnodigingenRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "TeamUitnodiging",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessTeamUitnodigingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "TeamUitnodigingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "TeamUitnodiging",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessTeamUitnodigingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "TeamUitnodigingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "TeamUitnodiging",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessTeamUitnodigingRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "TeamUitnodiging",
                        Method = "DELETE",
                        Procedure = "sp_dv_ProcessTeamUitnodigingRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name =  "TeamUitnodigingId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Tellingen",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessTellingenRequest",
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Overzicht",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessOverzichtRequest",
                        apiType = apiType
                    },                
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "HoogteMeters",
                        Method = "GET",
                        Procedure = "sp_dv_ProcessHoogteMetersRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "HoogteMetersId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "HoogteMeters",
                        Method = "PUT",
                        Procedure = "sp_dv_ProcessHoogteMetersRequest",
                        Parameters = new List<ApiParameter>() { new ApiParameter() { ParameterType= ParameterType.PrimaryKey, Name = "HoogteMetersId", Value = "" } },
                        apiType = apiType
                    },
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "HoogteMeters",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessHoogteMetersRequest",
                        apiType = apiType
                    },
                };
            }

            if (apiType == APIType.Questionnaire)
            {
                dataset = new List<ApiToStoredProcedure>()
                {
                    new ApiToStoredProcedure()
                    {
                        Endpoint = "Enquete",
                        Method = "POST",
                        Procedure = "sp_dv_ProcessEnqueteRequest",
                        apiType = apiType
                    }
                };
            }

            return dataset;
        }
    }
}